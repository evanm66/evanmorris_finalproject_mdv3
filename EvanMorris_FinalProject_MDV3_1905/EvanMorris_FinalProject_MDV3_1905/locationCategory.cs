﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;


namespace EvanMorris_FinalProject_MDV3_1905
{
    public partial class locationCategory : Form
    {

        public MySqlDataAdapter adr { get; set; }
        public MySqlConnection conn { get; set; }
        public DataTable locationDataTable = new DataTable();

        public locationCategory()
        {
            InitializeComponent();

            //calling method that guarantees the form will open to the proper size
            HandleClientWindowSize();
        }

        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        // Method to pull locations and switch views
        private bool getLocationView(int categoryId, string categoryName)
        {

            //Create SQL statement as a string
            string sql = "SELECT * FROM locations WHERE categoryID = " + categoryId + ";";

            //create data adapter
            adr = new MySqlDataAdapter(sql, conn);

            //set the type for the SELECT command... in this case it's text
            adr.SelectCommand.CommandType = CommandType.Text;

            //Fill method add the rows to mathc the data source
            adr.Fill(locationDataTable);

            //count the number of rows in the record set
            int numOfRows = locationDataTable.Select().Length;

            if (numOfRows < 1)
            {
                return false;
            }

            //MessageBox.Show("Got locations! " + numOfRows);

            //create category view and set it's data
            locations locationsView = new locations();
            locationsView.LocationList = new List<LocationObj>();
            locationsView.categoryName = categoryName;
            locationsView.conn = conn;
            locationsView.adr = adr;

            // loop through table data
            for (int i = 0; i < numOfRows; i++)
            {

                // create a location object
                string latString = locationDataTable.Rows[i]["latitude"].ToString();
                string longString = locationDataTable.Rows[i]["longitude"].ToString();

                LocationObj newLoc = new LocationObj();
                newLoc.Name = locationDataTable.Rows[i]["name"].ToString();

                float latitudeFloat;
                float longitudeFloat;

                float.TryParse(latString, out latitudeFloat);
                newLoc.Latitude = latitudeFloat;
                float.TryParse(longString, out longitudeFloat);
                newLoc.Longitude = longitudeFloat;

                // add location obj to view list
                locationsView.LocationList.Add(newLoc);

            }

            // show view
            locationsView.Show();
            this.Close();

            return true;
        }

        private void beachBtn_Click(object sender, EventArgs e)
        {
            getLocationView(1, "Beaches");
        }

        private void stateParkBtn_Click(object sender, EventArgs e)
        {
            getLocationView(2, "State Parks");
        }

        private void themeParkBtn_Click(object sender, EventArgs e)
        {
            getLocationView(3, "Theme Parks");
        }

        private void zooBtn_Click(object sender, EventArgs e)
        {
            getLocationView(4, "Zoos");
        }

        private void settingBtn_Click(object sender, EventArgs e)
        {
            Form2 settingsView = new Form2();
            settingsView.conn = conn;
            settingsView.adr = adr;
            settingsView.Show();
            this.Close();
        }
    }
}
