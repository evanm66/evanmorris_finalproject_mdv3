﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;


namespace EvanMorris_FinalProject_MDV3_1905
{
    class DBUtilities
    {
        //Method to create connection string.
        public static string BuildConnectionString()
        {
            //String to hold IP Address and Port
            string serverIP = "";
            string port = "";

            try
            {
                // open the text file using a StreamReader
                using (StreamReader sr = new StreamReader(@"C:\VFW\connect.txt"))
                {
                    //read the data from the text file
                    serverIP = sr.ReadLine();
                    port = sr.ReadLine();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Make sure servers are on!", "ERROR");
                // MessageBox.Show(e.ToString());
            }

            //return the connection string
            return $"server = {serverIP};uid=dbsAdmin;pwd=password;database=FloridaFuncast;SslMode=none;port={port};";

        }

        //method to create connection to MySql database
        public static MySqlConnection Connect(string myConnString)
        {
            //need connection object to send back
            MySqlConnection conn = new MySqlConnection();

            try
            {
                conn.ConnectionString = myConnString;
                conn.Open();
                //DEBUG messageBox to indicate successful connection
               //MessageBox.Show("Connected");
            }
            catch (MySqlException e)
            {

                switch (e.Number)
                {
                    case 1041:
                        MessageBox.Show("Can't resolve host address.\n\n" + myConnString);
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password.");
                        break;
                    default:
                        //exception error message
                        MessageBox.Show(e.ToString() + "\n\n" + myConnString);
                        break;
                }
            }

            return conn;
        }
    }
}
