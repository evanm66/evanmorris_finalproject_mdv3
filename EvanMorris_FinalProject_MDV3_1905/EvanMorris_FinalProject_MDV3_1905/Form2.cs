﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;

namespace EvanMorris_FinalProject_MDV3_1905
{
    // Evan Morris
    // MDV 3 1905
    // Final Project

    public partial class Form2 : Form
    {
        public MySqlDataAdapter adr { get; set; }
        public MySqlConnection conn { get; set; }

        public Form2()
        {
            InitializeComponent();
        }

        // Home button
        private void homeBtn_Click(object sender, EventArgs e)
        {
            locationCategory categoryView = new locationCategory();
            categoryView.conn = conn;
            categoryView.adr = adr;
            categoryView.Show();
            this.Close();
        }
    }
}
