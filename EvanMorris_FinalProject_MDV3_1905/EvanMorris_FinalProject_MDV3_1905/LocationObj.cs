﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvanMorris_FinalProject_MDV3_1905
{
    // Evan Morris
    // MDV 3 1905
    // Final Project

    public class LocationObj
    {

        public string Name { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}
