﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;


namespace EvanMorris_FinalProject_MDV3_1905
{
    public partial class Form1 : Form
    {

        MySqlDataAdapter adr;

        //public global of the database connection
        MySqlConnection conn = new MySqlConnection();

        //Global datatable
        DataTable theData = new DataTable();

        public Form1()
        {
            InitializeComponent();

            //calling method that guarantees the form will open to the proper size
            HandleClientWindowSize();


            //invoke the method to build the connection string
            string connString = DBUtilities.BuildConnectionString();


            //invoke the method to make ocnnection
            conn = DBUtilities.Connect(connString);

           // RetrieveData();

        }


        private bool loginUser( string username, string password)
        {
            //Create SQL statement as a string
            string sql = "SELECT * FROM users WHERE userName = '" + username + "' AND password = '" + password + "';";

            //create data adapter
            adr = new MySqlDataAdapter(sql, conn);

            //set the type for the SELECT command... in this case it's text
            adr.SelectCommand.CommandType = CommandType.Text;

            //Fill method add the rows to mathc the data source
            adr.Fill(theData);

            //count the number of rows in the record set
            int numOfRows = theData.Select().Length;

            // if rows are returned then a user exists with matching credentials
            if( numOfRows >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

       


        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        // Login Method
        private void btnLogin_Click(object sender, EventArgs e)
        {
           // Check db for user
           bool isLoggedIn = loginUser(userNameBox.Text, passwordBox.Text);

           if( isLoggedIn)
           {
                //MessageBox.Show("Logged In!");

                locationCategory categoryView = new locationCategory();
                categoryView.conn = conn;
                categoryView.adr = adr;
                categoryView.Show();
                //this.Close();

            }
           else
           {
               MessageBox.Show("Invalid login, please try again.");
           }


        }
    }

    
}
