﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvanMorris_FinalProject_MDV3_1905
{
    // Evan Morris
    // MDV 3 1905
    // Final Project

    public class LocationDetailsObj
    {

        public string Name { get; set; }
        public string Temp { get; set; }
        public string AppTemp { get; set; }
        public string PrecProb { get; set; }
        public string StormDist { get; set; }
    }
}
