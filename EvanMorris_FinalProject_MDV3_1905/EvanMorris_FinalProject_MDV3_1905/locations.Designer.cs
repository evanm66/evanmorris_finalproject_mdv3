﻿namespace EvanMorris_FinalProject_MDV3_1905
{
    partial class locations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingBtn = new System.Windows.Forms.Button();
            this.homeBtn = new System.Windows.Forms.Button();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.locationListView = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // settingBtn
            // 
            this.settingBtn.Location = new System.Drawing.Point(479, 200);
            this.settingBtn.Name = "settingBtn";
            this.settingBtn.Size = new System.Drawing.Size(98, 35);
            this.settingBtn.TabIndex = 6;
            this.settingBtn.Text = "settings";
            this.settingBtn.UseVisualStyleBackColor = true;
            this.settingBtn.Click += new System.EventHandler(this.settingBtn_Click);
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(258, 1016);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(150, 78);
            this.homeBtn.TabIndex = 8;
            this.homeBtn.Text = "Home";
            this.homeBtn.UseVisualStyleBackColor = true;
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(262, 271);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(146, 25);
            this.categoryLabel.TabIndex = 9;
            this.categoryLabel.Text = "locationTypes";
            // 
            // locationListView
            // 
            this.locationListView.Location = new System.Drawing.Point(154, 339);
            this.locationListView.Name = "locationListView";
            this.locationListView.Size = new System.Drawing.Size(372, 563);
            this.locationListView.TabIndex = 11;
            this.locationListView.UseCompatibleStateImageBehavior = false;
            this.locationListView.View = System.Windows.Forms.View.List;
            this.locationListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.locationListView_MouseClick);
            // 
            // locations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::EvanMorris_FinalProject_MDV3_1905.Properties.Resources.iPhone7Image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(661, 1280);
            this.Controls.Add(this.locationListView);
            this.Controls.Add(this.categoryLabel);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.settingBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "locations";
            this.Text = "zxxxxxxxx";
            this.Load += new System.EventHandler(this.locations_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button settingBtn;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ListView locationListView;
    }
}