﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;
//Add directive to include Newtonsoft
using Newtonsoft.Json.Linq;
//Add directive to include internet connectivity
using System.Net;
using System.IO;

namespace EvanMorris_FinalProject_MDV3_1905
{
    public partial class Details : Form
    {
        public MySqlDataAdapter adr { get; set; }
        public MySqlConnection conn { get; set; }
        public LocationDetailsObj DetailsObj { get; set; } 
        public Details()
        {
            InitializeComponent();
        }

        private void Details_Load(object sender, EventArgs e)
        {

            if( DetailsObj != null)
            {
                locationNameLabel.Text = DetailsObj.Name;
                tempTextBox.Text = DetailsObj.Temp + "F";
                appTempTextBox.Text = DetailsObj.AppTemp + "F";
                probPreciptextBox.Text = DetailsObj.PrecProb + "%";
                stormTextBox.Text = DetailsObj.StormDist + "mi";
            }
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            locationCategory categoryView = new locationCategory();
            categoryView.conn = conn;
            categoryView.adr = adr;
            categoryView.Show();
            this.Close();
        }

        private void settingBtn_Click(object sender, EventArgs e)
        {
            Form2 settingsView = new Form2();
            settingsView.conn = conn;
            settingsView.adr = adr;
            settingsView.Show();
            this.Close();
        }

        // Save details
        private void saveBtn_Click(object sender, EventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.FileName = "funcast.txt";
            savefile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(savefile.FileName))
                {

                    sw.WriteLine("Today's Funcast");
                    sw.WriteLine("Location: " + DetailsObj.Name);
                    sw.WriteLine("Temperature: " + DetailsObj.Temp + "F");
                    sw.WriteLine("Feels Like: " + DetailsObj.AppTemp + "F");
                    sw.WriteLine("Chance of Rain: " + DetailsObj.PrecProb + "%");
                    sw.WriteLine("Closest Storm: " + DetailsObj.StormDist + "mi");

                }

            }
        }
    }
}
