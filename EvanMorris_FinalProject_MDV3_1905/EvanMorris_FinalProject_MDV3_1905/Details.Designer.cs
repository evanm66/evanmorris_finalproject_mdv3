﻿namespace EvanMorris_FinalProject_MDV3_1905
{
    partial class Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.stormTextBox = new System.Windows.Forms.TextBox();
            this.appTempTextBox = new System.Windows.Forms.TextBox();
            this.tempTextBox = new System.Windows.Forms.TextBox();
            this.probPreciptextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.locationNameLabel = new System.Windows.Forms.Label();
            this.homeBtn = new System.Windows.Forms.Button();
            this.settingBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.saveBtn);
            this.groupBox1.Controls.Add(this.stormTextBox);
            this.groupBox1.Controls.Add(this.appTempTextBox);
            this.groupBox1.Controls.Add(this.tempTextBox);
            this.groupBox1.Controls.Add(this.probPreciptextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(99, 297);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(477, 657);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Forecast";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(159, 466);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(150, 63);
            this.saveBtn.TabIndex = 8;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // stormTextBox
            // 
            this.stormTextBox.Location = new System.Drawing.Point(214, 273);
            this.stormTextBox.Name = "stormTextBox";
            this.stormTextBox.ReadOnly = true;
            this.stormTextBox.Size = new System.Drawing.Size(204, 31);
            this.stormTextBox.TabIndex = 7;
            // 
            // appTempTextBox
            // 
            this.appTempTextBox.Location = new System.Drawing.Point(214, 147);
            this.appTempTextBox.Name = "appTempTextBox";
            this.appTempTextBox.ReadOnly = true;
            this.appTempTextBox.Size = new System.Drawing.Size(204, 31);
            this.appTempTextBox.TabIndex = 6;
            // 
            // tempTextBox
            // 
            this.tempTextBox.Location = new System.Drawing.Point(214, 81);
            this.tempTextBox.Name = "tempTextBox";
            this.tempTextBox.ReadOnly = true;
            this.tempTextBox.Size = new System.Drawing.Size(204, 31);
            this.tempTextBox.TabIndex = 5;
            // 
            // probPreciptextBox
            // 
            this.probPreciptextBox.Location = new System.Drawing.Point(214, 209);
            this.probPreciptextBox.Name = "probPreciptextBox";
            this.probPreciptextBox.ReadOnly = true;
            this.probPreciptextBox.Size = new System.Drawing.Size(204, 31);
            this.probPreciptextBox.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(155, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nearest Storm:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Chance of Rain:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Feels like:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Temperature:";
            // 
            // locationNameLabel
            // 
            this.locationNameLabel.AutoSize = true;
            this.locationNameLabel.Location = new System.Drawing.Point(265, 232);
            this.locationNameLabel.Name = "locationNameLabel";
            this.locationNameLabel.Size = new System.Drawing.Size(143, 25);
            this.locationNameLabel.TabIndex = 1;
            this.locationNameLabel.Text = "locationName";
            // 
            // homeBtn
            // 
            this.homeBtn.Location = new System.Drawing.Point(258, 1022);
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Size = new System.Drawing.Size(150, 78);
            this.homeBtn.TabIndex = 9;
            this.homeBtn.Text = "Home";
            this.homeBtn.UseVisualStyleBackColor = true;
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // settingBtn
            // 
            this.settingBtn.Location = new System.Drawing.Point(478, 184);
            this.settingBtn.Name = "settingBtn";
            this.settingBtn.Size = new System.Drawing.Size(98, 35);
            this.settingBtn.TabIndex = 10;
            this.settingBtn.Text = "settings";
            this.settingBtn.UseVisualStyleBackColor = true;
            this.settingBtn.Click += new System.EventHandler(this.settingBtn_Click);
            // 
            // Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::EvanMorris_FinalProject_MDV3_1905.Properties.Resources.iPhone7Image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(661, 1280);
            this.Controls.Add(this.settingBtn);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.locationNameLabel);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Details";
            this.Text = "Details";
            this.Load += new System.EventHandler(this.Details_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label locationNameLabel;
        private System.Windows.Forms.TextBox stormTextBox;
        private System.Windows.Forms.TextBox appTempTextBox;
        private System.Windows.Forms.TextBox tempTextBox;
        private System.Windows.Forms.TextBox probPreciptextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button homeBtn;
        private System.Windows.Forms.Button settingBtn;
        private System.Windows.Forms.Button saveBtn;
    }
}