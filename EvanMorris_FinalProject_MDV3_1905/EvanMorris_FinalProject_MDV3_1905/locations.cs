﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Add directive to connect to MySql
using MySql.Data.MySqlClient;
using System.IO;
//directive to throw MessageBox in case of exception
using System.Windows.Forms;
//Add directive to include Newtonsoft
using Newtonsoft.Json.Linq;
//Add directive to include internet connectivity
using System.Net;
using System.IO;

namespace EvanMorris_FinalProject_MDV3_1905
{
    public partial class locations : Form
    {
        public MySqlDataAdapter adr { get; set; }
        public MySqlConnection conn { get; set; }
        public List<LocationObj> LocationList { get; set; }
        public string categoryName { get; set; }
        //public DataTable locationDataTable { get; set; }

        //Variables to be used throughout the process
        WebClient apiConnection = new WebClient();
        //APi starting point
        string apiStart = "https://api.darksky.net/forecast/ba38a96a18d8cac0854adabe03ce7b6f/";
        //API string to hold completed API
        string apiEndPoint;

        public locations()
        {
            InitializeComponent();
        }

        private void locations_Load(object sender, EventArgs e)
        {

            if (LocationList != null && categoryName != null)
            {
                categoryLabel.Text = categoryName;
                foreach (var loc in LocationList)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = loc.ToString();
                    lvi.Tag = loc;
                    locationListView.Items.Add(lvi);
                }
            }
        }

        // Method to load detail view
        private void loadDetails( float latitude, float longitude, string locationName)
        {
            try
            {
                
                //ads the symbol code to the end of the global variable
                apiEndPoint = apiStart + latitude + "," + longitude;

                //MessageBox.Show(apiEndPoint);

                //Variable to hold the returned data
                var apiData = apiConnection.DownloadString(apiEndPoint);

                //string apiDataStr = Convert.ToString(apiData);
                //Parse the string to a JSON object
                JObject o = JObject.Parse(apiData);


                // create view
                Details detailView = new Details();

                // Set detail variables
                detailView.DetailsObj = new LocationDetailsObj();
                detailView.DetailsObj.Name = locationName;
                detailView.DetailsObj.Temp = o["currently"]["temperature"].ToString();
                detailView.DetailsObj.AppTemp = o["currently"]["apparentTemperature"].ToString();
                detailView.DetailsObj.PrecProb = o["currently"]["precipProbability"].ToString();
                detailView.DetailsObj.StormDist = o["currently"]["nearestStormDistance"].ToString();
                detailView.conn = conn;
                detailView.adr = adr;

                // show view
                detailView.Show();
                this.Close();
              
            }
            catch
            {
                MessageBox.Show("It ain't the end of the world. Check back again shortly.", "ERROR");
            }
        }

        private void locationListView_MouseClick(object sender, MouseEventArgs e)
        {
            // get selected location
            LocationObj selectedLoc = new LocationObj();
            selectedLoc = (LocationObj) locationListView.SelectedItems[0].Tag;

            // load details
            loadDetails(selectedLoc.Latitude, selectedLoc.Longitude, selectedLoc.Name);
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            locationCategory categoryView = new locationCategory();
            categoryView.Show();
            this.Close();

        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            locationCategory categoryView = new locationCategory();
            categoryView.conn = conn;
            categoryView.adr = adr;
            categoryView.Show();
            this.Close();
        }

        private void settingBtn_Click(object sender, EventArgs e)
        {
            Form2 settingsView = new Form2();
            settingsView.conn = conn;
            settingsView.adr = adr;
            settingsView.Show();
            this.Close();
        }
    }
}
