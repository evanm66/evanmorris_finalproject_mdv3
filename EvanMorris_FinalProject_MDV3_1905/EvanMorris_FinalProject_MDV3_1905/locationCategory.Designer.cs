﻿namespace EvanMorris_FinalProject_MDV3_1905
{
    partial class locationCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.themeParkBtn = new System.Windows.Forms.Button();
            this.zooBtn = new System.Windows.Forms.Button();
            this.stateParkBtn = new System.Windows.Forms.Button();
            this.beachBtn = new System.Windows.Forms.Button();
            this.settingBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.themeParkBtn);
            this.groupBox1.Controls.Add(this.zooBtn);
            this.groupBox1.Controls.Add(this.stateParkBtn);
            this.groupBox1.Controls.Add(this.beachBtn);
            this.groupBox1.Location = new System.Drawing.Point(126, 225);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 867);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Location Category";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "LOCATIONS";
            // 
            // themeParkBtn
            // 
            this.themeParkBtn.Location = new System.Drawing.Point(19, 429);
            this.themeParkBtn.Name = "themeParkBtn";
            this.themeParkBtn.Size = new System.Drawing.Size(169, 155);
            this.themeParkBtn.TabIndex = 3;
            this.themeParkBtn.Text = "Theme Parks";
            this.themeParkBtn.UseVisualStyleBackColor = true;
            this.themeParkBtn.Click += new System.EventHandler(this.themeParkBtn_Click);
            // 
            // zooBtn
            // 
            this.zooBtn.Location = new System.Drawing.Point(233, 429);
            this.zooBtn.Name = "zooBtn";
            this.zooBtn.Size = new System.Drawing.Size(169, 155);
            this.zooBtn.TabIndex = 2;
            this.zooBtn.Text = "Zoos";
            this.zooBtn.UseVisualStyleBackColor = true;
            this.zooBtn.Click += new System.EventHandler(this.zooBtn_Click);
            // 
            // stateParkBtn
            // 
            this.stateParkBtn.Location = new System.Drawing.Point(233, 234);
            this.stateParkBtn.Name = "stateParkBtn";
            this.stateParkBtn.Size = new System.Drawing.Size(169, 155);
            this.stateParkBtn.TabIndex = 1;
            this.stateParkBtn.Text = "State Parks";
            this.stateParkBtn.UseVisualStyleBackColor = true;
            this.stateParkBtn.Click += new System.EventHandler(this.stateParkBtn_Click);
            // 
            // beachBtn
            // 
            this.beachBtn.Location = new System.Drawing.Point(19, 234);
            this.beachBtn.Name = "beachBtn";
            this.beachBtn.Size = new System.Drawing.Size(169, 155);
            this.beachBtn.TabIndex = 0;
            this.beachBtn.Text = "Beaches";
            this.beachBtn.UseVisualStyleBackColor = true;
            this.beachBtn.Click += new System.EventHandler(this.beachBtn_Click);
            // 
            // settingBtn
            // 
            this.settingBtn.Location = new System.Drawing.Point(475, 174);
            this.settingBtn.Name = "settingBtn";
            this.settingBtn.Size = new System.Drawing.Size(98, 35);
            this.settingBtn.TabIndex = 5;
            this.settingBtn.Text = "settings";
            this.settingBtn.UseVisualStyleBackColor = true;
            this.settingBtn.Click += new System.EventHandler(this.settingBtn_Click);
            // 
            // locationCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::EvanMorris_FinalProject_MDV3_1905.Properties.Resources.iPhone7Image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(661, 1280);
            this.Controls.Add(this.settingBtn);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "locationCategory";
            this.Text = "locationCategory";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button themeParkBtn;
        private System.Windows.Forms.Button zooBtn;
        private System.Windows.Forms.Button stateParkBtn;
        private System.Windows.Forms.Button beachBtn;
        private System.Windows.Forms.Button settingBtn;
    }
}